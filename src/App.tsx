import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
//import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import Creature from './creature/creature';
import TitleScreen from './titlescreen/titlescreen';

const App: React.FC = () => {
  return (
    <Router>
      <Route path="/" exact component={TitleScreen} />
      <Route path="/Creature" exact component={Creature} />
    </Router>
  );
}

export default App;
