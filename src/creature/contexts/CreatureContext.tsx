import React from 'react'

export let CreatureList = {
  creatures: [
    {
      "name": "Filly",
      "height": 180,
      "weight": 140,
      "gender": "male"
    }
]}

const CreatureContext = React.createContext(CreatureList)
export default CreatureContext