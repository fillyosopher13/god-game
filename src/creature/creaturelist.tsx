import React, { Component } from 'react'
import CreatureContext from './contexts/CreatureContext';
import {CreatureList as Creatures} from './contexts/CreatureContext';

class CreatureList extends Component {
  render() {
    return (
      <CreatureContext.Provider value={Creatures}>
        <CreatureContext.Consumer>{
          ({creatures}) => {
            return <div>
              <h1>{creatures[0].name}</h1>
              <p>{creatures[0].gender}</p>
            </div>
          }
        }</CreatureContext.Consumer>
      </CreatureContext.Provider>
    );
  }
}

export default CreatureList;