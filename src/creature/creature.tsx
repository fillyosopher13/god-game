import React, { Component } from 'react'
import CreatureContext from './contexts/CreatureContext';
import {CreatureList} from './contexts/CreatureContext';

class Creature extends Component {
  render() {
    return (
      <CreatureContext.Provider value={CreatureList}>
        <CreatureContext.Consumer>{
          ({creatures}) => {
            return <div>
              <h1>{creatures[0].name}</h1>
              <p>{creatures[0].gender}</p>
            </div>
          }
        }</CreatureContext.Consumer>
      </CreatureContext.Provider>
    );
  }
}

export default Creature;