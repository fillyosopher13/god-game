import React, { Component } from 'react'
import CreatureList from '../creature/creaturelist';

class TitleScreen extends Component {
  render() {
    return (<div>
      <h2>Welcome to God Game (preliminary title)</h2>
      <h4>Version 0.0.1 - PreAlpha</h4>
      <p>
        Simple Erogame in the style of Free Cities
         (but hopefully easier to mod)
      </p>
      <p>
        This game has (or will soon have) adult elements not suitable for
        people under the age of 18.
      </p>
      <CreatureList />
    </div>)
  }
}

export default TitleScreen